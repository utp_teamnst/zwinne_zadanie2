package app.repository;

import app.model.Student;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository {
    List<Student> findAll();

    Student findByEmail(String email);
}
