package app.repository;


import app.model.Projekt;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface ProjektRepository extends JpaRepository<Projekt, Long> {
    Page<Projekt> findAllByOrderByProjektIdAsc(Pageable pageable);
    Projekt findByProjektId(Integer projektId);

    @Transactional
    void deleteByProjektId(Integer projektId);
}
