package app.tasks;

import app.model.Projekt;
import app.repository.ProjektRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/Chat")
public class Chat {
    @Autowired
    private ProjektRepository projektRepository;

    @GetMapping
    public String getAllProjects(HttpServletRequest request, Model model) {

        return "chat";
    }
}
