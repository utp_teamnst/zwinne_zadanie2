package app.tasks;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
public class ProjektUpload {

    
    private static String UPLOADED_FOLDER = "uploadedFiles/";

    @GetMapping("/ProjektUpload")
    public String index() {
        return "projekt_upload";
    }

    @PostMapping("/upload")
    public String singleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) {

        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Wybierz plik, ktory chcesz wrzucic");
            return "redirect:uploadStatus";
        }

        try {

            
            byte[] bytes = file.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
            Files.write(path, bytes);

            redirectAttributes.addFlashAttribute("message",
                    "Poprawnie wrzucono '" + file.getOriginalFilename() + "'");
            redirectAttributes.addFlashAttribute("message1",
                    "Sciezka do pliku to: '" + UPLOADED_FOLDER + file.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "redirect:/uploadStatus";
    }

    @GetMapping("/uploadStatus")
    public String uploadStatus() {
        return "projekt_uploadStatus";
    }
    
    @GetMapping("/ProjektPowrot")
    public String projektPowrot() {
    	return "projekt_upload";
    }
    
    @GetMapping("/ProjektFile")
    public String projektFile() {
    	return "projekt_upload";
    }

}
