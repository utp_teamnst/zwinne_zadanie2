package app.tasks;

import app.model.Projekt;
import app.repository.ProjektRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.time.LocalDateTime;

@Controller
@RequestMapping("/ProjektDodaj")
public class ProjektDodaj {
    @Autowired
    private ProjektRepository projektRepository;

    @GetMapping
    public String showForm(Model model){
        model.addAttribute("projekt", new Projekt());

        return "projekt_dodaj";
    }

    @PostMapping
    public RedirectView addProject(@ModelAttribute Projekt projekt, RedirectAttributes attributes){
        System.out.println("POST METHOD: " + projekt.getNazwa()+ projekt.getOpis());
        projekt.setDataCzasUtworzenia(LocalDateTime.now());
        projektRepository.save(projekt);
        attributes.addAttribute("id_add", projekt.getProjektId());
        System.out.println("ID utworzonego projektu: " + projekt.getProjektId());
        return new RedirectView("/ProjektLista");
    }
}
