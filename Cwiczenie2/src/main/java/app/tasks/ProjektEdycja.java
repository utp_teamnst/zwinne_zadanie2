package app.tasks;

import app.model.Projekt;
import app.repository.ProjektRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/ProjektEdycja")
public class ProjektEdycja {
    @Autowired
    ProjektRepository projektRepository;

    @GetMapping
    public String showEditForm(HttpServletRequest request, Model model) {
        Projekt projekt = new Projekt();
        if(request.getParameter("projekt_id") != null) {
            projekt = projektRepository.findByProjektId(Integer.parseInt(request.getParameter("projekt_id")));
        }
        System.out.println("GET: " + projekt.getProjektId());
        model.addAttribute(projekt);

        return "projekt_edycja";
    }

    @PostMapping
    public RedirectView editProject(@ModelAttribute Projekt projekt, RedirectAttributes attributes) {
        System.out.println("EDIT POST: " + projekt.getProjektId() + " " + projekt.getNazwa());
        projektRepository.save(projekt);
        return new RedirectView("/ProjektLista");
    }
}
