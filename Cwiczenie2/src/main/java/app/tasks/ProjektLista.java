package app.tasks;

import app.model.Projekt;
import app.repository.ProjektRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/ProjektLista")
public class ProjektLista {
    @Autowired
    private ProjektRepository projektRepository;

    @GetMapping
    public String getAllProjects(HttpServletRequest request, Model model) {
        int page = 0;
        int size = 10;

        if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
            page = Integer.parseInt(request.getParameter("page")) - 1;
        }

        Page<Projekt> projects = projektRepository.findAllByOrderByProjektIdAsc(PageRequest.of(page, size));

        model.addAttribute("projects", projects);
        return "projekt_lista";
    }
}
