package app.tasks;

import app.repository.ProjektRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

@Controller
@RequestMapping("/ProjektUsun")
public class ProjektUsun {
    @Autowired
    private ProjektRepository projektRepository;

    @PostMapping
    public RedirectView deleteProject(HttpServletRequest request, RedirectAttributes attributes) {
        System.out.println("Usuwanie");
        Enumeration<String> params = request.getParameterNames();
        while(params.hasMoreElements()) {
            System.out.println(params.nextElement());
        }

        if(request.getParameter("projekt_id") != null) {
          projektRepository.deleteByProjektId(Integer.parseInt(request.getParameter("projekt_id")));
        }
        attributes.addAttribute("id_delete", request.getParameter("projekt_id"));

        return new RedirectView("/ProjektLista");
    }
}
