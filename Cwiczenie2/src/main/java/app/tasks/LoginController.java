package app.tasks;

import app.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.xml.ws.RequestWrapper;

@Controller
public class LoginController implements WebMvcConfigurer {
    @GetMapping("/login")
    public String login(Model model)
    {
        model.addAttribute("msgs", messageRepository.findAll());
        return "login";
    }
    @Autowired
    private MessageRepository messageRepository;
}
