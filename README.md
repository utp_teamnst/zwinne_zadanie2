# Zwinne_zadanie2

1. Przejście na framework SpringBoot, w tym podział aplikacji webowej na dwa projekty Gradle'a np:
   * projekt-rest-api (REST API, model, repository/service, controller, tests - Mockito,MockMvc),
   * projekt-rest-client(views in Thymeleaf, model, service - RestTemplate, controller, tests).
2. Walidacja formularzy (np. wykorzystując adnotacje z pakietu javax.validation.constraints).
3. Komunikator korzystający z technologii WebSocket, oparty na frameworku Atmosphere,
4. Pobieranie i wgrywanie plików projektu (ścieżki składowane w bazie, pliki na dysku),
5. Utworzenie własnego certyfikatu SSL/TLS i przejście na połączenie szyfrowane - HTTPS,
6. Logowanie, uprawnienia itp