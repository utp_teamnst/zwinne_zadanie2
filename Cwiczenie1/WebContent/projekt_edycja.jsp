<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="app.model.Projekt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="styles.css">
<title>Edycja projektu</title>
</head>
<body>
<h2>Edytuj projekt</h2>
<form action="ProjektEdycja" method="POST">
	<table>
	<tr>
	<% 
	Projekt projekt = (Projekt) request.getAttribute("projekt"); 
	%>
	<input type="hidden" name="projekt_id" value="<%= projekt.getProjektId() %>">
		<td>Nazwa projektu:</td> <td><input name="field_nazwa" value="<%=projekt.getNazwa()%>"></td>
	</tr>
	<tr>
		<td>Opis projektu: </td><td><input name="field_opis" value="<%=projekt.getOpis()%>"></td>
	</tr>
	<tr>
		<td>Data oddania projektu: </td><td><input type="datetime-local" value="<%=projekt.getDataOddania()%>" name="field_data"></td>
	</tr>
<!--  	<tr>
		<td>Data utworzenia: </td><td><input type="date" name="date_creation" placeholder="Zaznacz datę utworzenia projektu"></td>
-->
		<td> </td><td><input name="btn_aktualizacja" value="Zaktualizuj" type="submit"></td>
	</table>
</form>
</body>
</html>