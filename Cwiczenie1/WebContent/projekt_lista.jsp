<%@page import="app.model.Projekt"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="styles.css">
<title>Lista projektów</title>
<style>
table {
  border-collapse: collapse;
}

table, td, th {
  border: 1px solid black;
}
</style>
</head>
<body>
<% if(request.getAttribute("id") !=null){
	%>
<h5> Brawo ! Dodany projekt otrzymał id = <%= request.getAttribute("id") %></h5>

<% }
else if(request.getAttribute("id_deleted") != null){
%>
<h5>Brawo ! Usunięto projekt o id= <%=request.getAttribute("id_deleted") %></h5>
<% } 

else if(request.getAttribute("edited_id") != null){
	%>
	<h5>Brawo ! Edytowano projekt o id= <%=request.getAttribute("edited_id") %></h5>
	<%
}%>


	
<h1>Lista projektów:</h1>
<table>
	<tr>
		<td>Wyszukaj projekt</td>
		<td>
		<form action="ProjektWyszukaj" method="Post">
			<input name="nazwa" type="text" >
			<input type="submit" name="btn_szukaj" value="Znajdz projekt">
		</form>
		</td>
	</tr>
</table>
</br>
<table border="1">
<tr>
<td><b>Nazwa</b></td>
<td><b>Opis</b></td>
<td><b>Data Utworzenia</b></td>
<td><b>Usuń</b></td>
</tr>
<%
	List<Projekt> projects = (List<Projekt>)request.getAttribute("projects_name");
	int strona, paginacja_start, paginacja_stop;
	try {
		strona = Integer.valueOf(request.getParameter("strona"));
	} catch (Exception e) {
		strona = 0;
	}

	if (projects != null) {
		if ((request.getParameter("btn_pokaz") != null) && (request.getParameter("btn_pokaz").equals("Pokaż wszystkie"))) {
			paginacja_start = 0;
			paginacja_stop = projects.size();
		} else if ((strona == 0) || (strona == 1)) {
			paginacja_start = 0;
			if (projects.size() <= 10) {
				paginacja_stop = projects.size();
			} else {
				paginacja_stop = 10;
			}
		} else {
			paginacja_start = strona * 10 - 10;
			if (paginacja_start + 10 < projects.size()) {
				paginacja_stop = paginacja_start + 10;
			} else {
				paginacja_stop = projects.size();
			}
		}
		for (int i = paginacja_start; i < paginacja_stop; i = i + 1) {
			Projekt project = projects.get(i);
%>
<tr><td><%= project.getNazwa() %></td>
<td><%= project.getOpis() %></td>
<td><%= project.getDataCzasUtworzenia().toString() %></td>
<td>
	<form action="ProjektLista" method="Post">
		<input type="hidden" name="projekt_id" value="<%= project.getProjektId() %>">
    	<input name="btn_usun" type="submit" value="Usun">
    	<input name="btn_edytuj" type="submit" value="Edytuj">
	</form>
</td>
</tr>
<%
		}
%>
</table>
Strony: 
<%
		int liczba_stron = projects.size() / 10 + 1;
		if (projects.size() % 10 > 0) {
			liczba_stron = liczba_stron + 1;
		}
		for (int i = 1; i < liczba_stron; i = i + 1) {
			if (strona == i) {
				%>
				<b>
				<%
			}
			%>
			<a href="ProjektLista?strona=<%= i %>"><%= i %> </a>
			<%
			if (strona == i) {
				%>
				</b>
				<%
			}
		}
	}
%>
<form action="ProjektDodaj" method="Get">
	<input name="btn_dodaj" type="submit" value="Dodaj nowy">
</form>
<form action="ProjektLista" method="Get">
	<input name="btn_pokaz" type="submit" value="Pokaż wszystkie">	
</form>
</body>
</html>