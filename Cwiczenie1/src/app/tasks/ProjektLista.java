package app.tasks;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.model.Projekt;
import app.util.HibernateUtil;

/**
 * Servlet implementation class ProjektLista
 */
@WebServlet("/ProjektLista")
public class ProjektLista extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProjektLista() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			ServletContext context = getServletContext();
			List<Projekt> projects = getProjectsList();
			request.setAttribute("projects_name", projects);
			RequestDispatcher dispatcher = context.getRequestDispatcher("/projekt_lista.jsp");
			dispatcher.forward(request, response); 		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		ServletContext context = getServletContext();
		if(request.getParameter("btn_dodaj") != null)
		{
			RequestDispatcher dispatcher = context.getRequestDispatcher("/ProjektDodaj");
			dispatcher.forward(request, response); 
		}
		else if(request.getParameter("btn_usun") != null)
		{
			RequestDispatcher dispatcher = context.getRequestDispatcher("/ProjektUsun");
			dispatcher.forward(request, response); 
		}
		else if(request.getParameter("btn_edytuj") != null)
		{
			RequestDispatcher dispatcher = context.getRequestDispatcher("/ProjektEdycja");
			dispatcher.forward(request, response); 
		}
		
		else {
		doGet(request, response);
		}

	}
	
	private List<Projekt> getProjectsList() {
		EntityManager entityManager = HibernateUtil.getInstance().createEntityManager();
		TypedQuery<Projekt> query = entityManager
				.createQuery("SELECT p FROM Projekt p", Projekt.class);
		List<Projekt> projekty = query.getResultList();
		entityManager.close();

		return projekty;
	}

}
