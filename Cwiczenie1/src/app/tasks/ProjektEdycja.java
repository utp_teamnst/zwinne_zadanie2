package app.tasks;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.model.Projekt;
import app.util.HibernateUtil;

/**
 * Servlet implementation class ProjektEdycja
 */
@WebServlet("/ProjektEdycja")
public class ProjektEdycja extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String CONTENT_TYPE = "text/html;charset=UTF-8";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProjektEdycja() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	   ServletContext context = getServletContext();
	   RequestDispatcher dispatcher = context.getRequestDispatcher("/projekt_edycja.jsp");
		EntityManager entityManager = HibernateUtil.getInstance().createEntityManager();
		TypedQuery<Projekt> query = entityManager
				.createQuery("SELECT p "
						+ "FROM Projekt p "
						+ "where p.id = :id", Projekt.class);
		query.setParameter("id", Integer.valueOf(request.getParameter("projekt_id")));
		Projekt projekt = query.getSingleResult();
		request.setAttribute("projekt",projekt);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		if(request.getParameter("btn_aktualizacja") != null) {
			EntityManager entityManager = HibernateUtil.getInstance().createEntityManager();
			
			int id = Integer.valueOf(request.getParameter("projekt_id"));
			Projekt projekt = entityManager.find(Projekt.class, id);
			request.setAttribute("project", projekt);
			entityManager.getTransaction().begin();
			Query q=  entityManager.createQuery("UPDATE Projekt as p "
					+ "SET p.nazwa = :name,"
					+ "p.opis = :opis,"
					+ "p.dataOddania = :data"
					+ " WHERE "
					+ "p.id = :id")
			.setParameter("id", id)
			.setParameter("name", request.getParameter("field_nazwa"))
			.setParameter("opis", request.getParameter("field_opis"))
			.setParameter("data", LocalDate.parse(request.getParameter("field_data")));

			int rowsUpdated=q.executeUpdate();
			entityManager.getTransaction().commit();
			entityManager.close();
			entityManager = HibernateUtil.getInstance().createEntityManager();
			request.setAttribute("edited_id", projekt.getProjektId());
			TypedQuery<Projekt>query = entityManager
					.createQuery("SELECT p FROM Projekt p", Projekt.class);
			List<Projekt> projekty = query.getResultList();
			request.setAttribute("projects_name", projekty);
			
		   ServletContext context = getServletContext();
		   RequestDispatcher dispatcher = context.getRequestDispatcher("/projekt_lista.jsp");

		   dispatcher.forward(request, response);
		}
		else if(request.getParameter("btn_edytuj") != null) {
			doGet(request, response);
		}



	}
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)  
			throws ServletException, IOException {
		response.setContentType(CONTENT_TYPE);

	}
		
	


	
}
