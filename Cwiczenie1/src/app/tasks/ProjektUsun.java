package app.tasks;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.model.Projekt;
import app.util.HibernateUtil;

/**
 * Servlet implementation class ProjektUsun
 */
@WebServlet("/ProjektUsun")
public class ProjektUsun extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProjektUsun() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getParameter("btn_usun") != null) {
			if(request.getParameter("projekt_id") != null) {
				deleteProject(Integer.parseInt(request.getParameter("projekt_id")));
			}
			request.setAttribute("id_deleted", request.getParameter("projekt_id"));
			EntityManager entityManager = HibernateUtil.getInstance().createEntityManager();
			TypedQuery<Projekt> query = entityManager
					.createQuery("SELECT p FROM Projekt p", Projekt.class);
			List<Projekt> projekty = query.getResultList();
			entityManager.close();
			ServletContext context = getServletContext();
			request.setAttribute("projects_name", projekty);
			RequestDispatcher dispatcher = context.getRequestDispatcher("/projekt_lista.jsp");
			dispatcher.forward(request, response); 
		}
//		else if (request.getParameter("btn_dodaj") != null) {
//			ServletContext context = getServletContext();
//			RequestDispatcher dispatcher = context.getRequestDispatcher("/projekt_edycja.jsp");
//			dispatcher.forward(request, response); 			
//		}
	}
	
	private void deleteProject(int projektId) {
		EntityManager entityManager = HibernateUtil.getInstance().createEntityManager();
		Projekt projekt = entityManager.find(Projekt.class, projektId);
		if(projekt != null) {
			entityManager.getTransaction().begin();
			entityManager.remove(projekt);
			entityManager.getTransaction().commit();
		}
	}

}
