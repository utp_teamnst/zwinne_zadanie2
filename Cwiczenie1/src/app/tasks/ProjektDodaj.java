package app.tasks;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.persistence.EntityManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.model.Projekt;
import app.util.HibernateUtil;

/**
 * Servlet implementation class ProjektEdycja
 */
@WebServlet("/ProjektDodaj")
public class ProjektDodaj extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String CONTENT_TYPE = "text/html;charset=UTF-8";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProjektDodaj() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ServletContext context = getServletContext();
		RequestDispatcher dispatcher = context.getRequestDispatcher("/projekt_dodaj.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
//		doGet(request, response);
		if(request.getParameter("btn_zapisz") != null) {
			EntityManager entityManager = HibernateUtil.getInstance().createEntityManager();
			Projekt projekt = new Projekt();
			
			projekt.setNazwa(request.getParameter("field_nazwa"));
			projekt.setDataCzasUtworzenia(LocalDateTime.now());
			projekt.setDataOddania(LocalDate.now().plusMonths(2));
			projekt.setOpis(request.getParameter("field_opis"));
			entityManager.getTransaction().begin();
			entityManager.persist(projekt);
			entityManager.getTransaction().commit();
			entityManager.close();
			request.setAttribute("projekt", projekt);
			request.setAttribute("id", projekt.getProjektId());
			}
	   ServletContext context = getServletContext();
	   RequestDispatcher dispatcher = context.getRequestDispatcher("/ProjektLista");
	   
	   dispatcher.forward(request, response);


	}
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)  
			throws ServletException, IOException {
		response.setContentType(CONTENT_TYPE);

	}
		
	


	
}
