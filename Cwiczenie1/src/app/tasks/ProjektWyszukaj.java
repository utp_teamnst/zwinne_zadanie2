package app.tasks;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.model.Projekt;
import app.util.HibernateUtil;

/**
 * Servlet implementation class ProjektWyszukaj
 */
@WebServlet("/ProjektWyszukaj")
public class ProjektWyszukaj extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProjektWyszukaj() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ServletContext context = getServletContext();
		String name = request.getParameter("nazwa");
		if(name != null && name != "") {
			EntityManager entityManager = HibernateUtil.getInstance().createEntityManager();
			TypedQuery<Projekt> query = entityManager
					.createQuery("SELECT p "
							+ "FROM Projekt p "
							+ "where lower(p.nazwa) LIKE lower(:nazwa) "
							+ "OR "
							+ "lower(p.opis) like lower(:opis)", Projekt.class);
			query.setParameter("nazwa", "%"+name+"%");
			query.setParameter("opis", "%"+name+"%");
			List<Projekt> projekty = query.getResultList();
			entityManager.close();
		
		RequestDispatcher dispatcher = context.getRequestDispatcher("/projekt_lista.jsp");
		request.setAttribute("projects_name", projekty);
		dispatcher.forward(request, response);
		}
		
	}

}
