package app.listeners;

import javax.servlet.ServletContextEvent;
import app.util.HibernateUtil;
import javax.servlet.ServletContextListener;

public class ApplicationContextListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		HibernateUtil.getInstance().closeEntityManagerFactory();

	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		HibernateUtil.getInstance().createEntityManagerFactory();

	}

}
